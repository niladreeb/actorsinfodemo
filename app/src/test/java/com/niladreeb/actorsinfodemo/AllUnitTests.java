package com.niladreeb.actorsinfodemo;

import com.niladreeb.actorsinfodemo.actorslist.ActorListPresenterTest;
import com.niladreeb.actorsinfodemo.data.ActorsServiceTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by niladreeb on 24/11/2017.
 * Run all unit tests
 */

@RunWith(Suite.class)
@Suite.SuiteClasses(value = {
        ActorsServiceTest.class,
        ActorListPresenterTest.class
})
public class AllUnitTests {
}