package com.niladreeb.actorsinfodemo.actorslist;

import com.niladreeb.actorsinfodemo.data.ActorsService;
import com.niladreeb.actorsinfodemo.model.Actor;
import com.niladreeb.actorsinfodemo.model.ActorsResponse;
import com.niladreeb.actorsinfodemo.presentation.ui.actorslist.ActorListContract;
import com.niladreeb.actorsinfodemo.presentation.ui.actorslist.ActorListPresenter;
import com.niladreeb.actorsinfodemo.rules.RxJavaTestRule;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by niladreeb on 24/11/2017.
 * Presenter layer test for {@link ActorListPresenter}
 */

public class ActorListPresenterTest extends RxJavaTestRule {

    @Mock
    private ActorListContract.View mMockView;

    @Mock
    private ActorsService mMockActorService;

    private ActorListPresenter mActorListPresenter;

    /**
     * Mockito setup
     */
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mActorListPresenter = new ActorListPresenter(mMockActorService, mMockView);
    }

    /**
     * Test error response for {@link ActorsService#getActors()} ()}
     */
    @Test
    public void testShowsErrorIfRequestError() {
        String errorMsg = "Error message";

        when(mMockActorService.getActors()).thenReturn(Observable.error(new Exception(errorMsg)));

        mActorListPresenter.getActorsList();

        verify(mMockView).displayErrorResponse(eq(errorMsg));
    }

    /**
     * Test success response for {@link ActorsService#getActors()}
     */
    @Test
    public void testLoadActorsMethodIsCalledIfRequestSuccess() {
        when(mMockActorService.getActors()).thenReturn(Observable.just(getActorsResponse()));

        mActorListPresenter.getActorsList();

        verify(mMockView).loadActors(any());
    }

    /**
     * Mock data
     * @return {@link ActorsResponse}
     */
    private ActorsResponse getActorsResponse() {
        ActorsResponse actorsResponse = new ActorsResponse();
        List<Actor> actors = new ArrayList<>();
        Actor actor = new Actor();
        actor.setmName("Test Actor");
        actor.setmDescription("Test Description");
        actor.setmDob("Test Dob");
        actor.setmCountry("Test Country");
        actor.setmHeight("Test Height");
        actor.setmSpouse("Test Spouse");
        actor.setmChildren("Test Children");
        actor.setmImage("Test Image Url");
        actors.add(actor);
        actorsResponse.setmActor(actors);

        return actorsResponse;
    }
}
