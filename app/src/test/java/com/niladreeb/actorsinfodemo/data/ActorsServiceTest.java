package com.niladreeb.actorsinfodemo.data;

import com.niladreeb.actorsinfodemo.model.ActorsResponse;
import com.niladreeb.actorsinfodemo.rules.RxJavaTestRule;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Observable;
import io.reactivex.Observer;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import okio.BufferedSource;
import retrofit2.Response;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by niladreeb on 24/11/2017.
 * Service layer test for {@link ActorsService}
 */

public class ActorsServiceTest extends RxJavaTestRule {

    @Mock
    ActorsApi mActorsApi;

    @Mock
    Observer<ActorsResponse> mMockGetActorsSubscriber;

    private ActorsService mActorsService;

    /**
     * Mockito setup
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mActorsService = new ActorsService(mActorsApi);
    }

    /**
     * Test error response for {@link ActorsService#getActors()}
     */
    @Test
    public void testGetActorsServerErrorCausesRxError() {
        Response<ActorsResponse> errorResponse = Response.error(501, emptyResponseBody());

        when(mActorsApi.getActors()).thenReturn(Observable.just(errorResponse));

        mActorsService.getActors().subscribe(mMockGetActorsSubscriber);

        verify(mMockGetActorsSubscriber).onError(any());
    }

    /**
     * Test success response for {@link ActorsService#getActors()}
     */
    @Test
    public void testGetActorsSuccessResponseIsReceivedBySubscriber() {
        ActorsResponse actorsResponse = new ActorsResponse();

        Response<ActorsResponse> successResponse = Response.success(actorsResponse);

        when(mActorsApi.getActors()).thenReturn(Observable.just(successResponse));

        // Subscribing in order to trigger the observable call. This is necessary since we use defer() in service
        mActorsService.getActors().subscribe(mMockGetActorsSubscriber);

        verify(mMockGetActorsSubscriber).onNext(eq(actorsResponse));
    }

    /**
     * Mock response body
     * @return
     */
    private static ResponseBody emptyResponseBody() {
        return new ResponseBody() {
            @Override
            public MediaType contentType() {
                return null;
            }

            @Override
            public long contentLength() {
                return 0;
            }

            @Override
            public BufferedSource source() {
                return null;
            }
        };
    }
}
