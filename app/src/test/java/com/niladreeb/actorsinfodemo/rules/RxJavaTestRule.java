package com.niladreeb.actorsinfodemo.rules;

import org.junit.After;
import org.junit.Before;

import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by niladreeb on 24/11/2017.
 * RxJava and RxAndroid test rules
 */

public class RxJavaTestRule {

	@Before
	public void rxSetup() {
		setAllRxThreadsToMainThread();
	}

	private void setAllRxThreadsToMainThread() {
		RxAndroidPlugins.reset();
		RxJavaPlugins.reset();

		RxJavaPlugins.setIoSchedulerHandler(scheduler -> Schedulers.trampoline());
		RxJavaPlugins.setComputationSchedulerHandler(scheduler -> Schedulers.trampoline());
		RxJavaPlugins.setNewThreadSchedulerHandler(scheduler -> Schedulers.trampoline());
		RxAndroidPlugins.setMainThreadSchedulerHandler(schedulerCallable -> Schedulers.trampoline());
		RxAndroidPlugins.setInitMainThreadSchedulerHandler(scheduler -> Schedulers.trampoline());
	}

	@After
	public void tearDown() throws Exception {
		RxAndroidPlugins.reset();
		RxJavaPlugins.reset();
	}}