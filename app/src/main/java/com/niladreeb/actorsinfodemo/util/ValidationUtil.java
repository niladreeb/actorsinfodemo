package com.niladreeb.actorsinfodemo.util;

import android.support.annotation.Nullable;

/**
 * Created by niladree on 24/11/2017.
 */

public class ValidationUtil {

    private ValidationUtil() {
        throw new AssertionError();
    }

    /**
     * Validation method to check null pointer exception
     * @param reference class
     * @param errorMessage string
     * @param <T>
     * @return
     */
    public static <T> T checkNotNull(T reference, @Nullable Object errorMessage) {
        if(reference == null) {
            throw new NullPointerException(String.valueOf(errorMessage));
        } else {
            return reference;
        }
    }
}
