package com.niladreeb.actorsinfodemo.presentation.view;

/**
 * Created by niladreeb on 24/11/2017.
 */

public interface BaseView<T> {

    /**
     * Show a loading view with a progress bar indicating a loading process.
     */
    void showLoading();

    /**
     * Hide a loading view.
     */
    void hideLoading();

    /**
     * Display error in snackbar
     * @param errorMessage string
     */
    void displayErrorResponse(String errorMessage);
}
