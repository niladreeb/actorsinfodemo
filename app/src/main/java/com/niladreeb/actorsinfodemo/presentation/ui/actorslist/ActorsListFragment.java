package com.niladreeb.actorsinfodemo.presentation.ui.actorslist;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.niladreeb.actorsinfodemo.R;
import com.niladreeb.actorsinfodemo.data.ActorsService;
import com.niladreeb.actorsinfodemo.di.Injector;
import com.niladreeb.actorsinfodemo.model.Actor;
import com.niladreeb.actorsinfodemo.model.ActorsResponse;
import com.niladreeb.actorsinfodemo.presentation.uicommon.SingleFragmentActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by niladreeb on 24/11/2017.
 */

public class ActorsListFragment extends Fragment implements ActorListContract.View{

    private static final String TAG = ActorsListFragment.class.getSimpleName();

    @Inject
    ActorsService mActorsService;

    @BindView(R.id.rootView)
    ViewGroup mRootView;
    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.actors_rv)
    RecyclerView mActorRecyclerView;

    private ActorListContract.Presenter mPresenter;
    private ActorListRecyclerAdapter mAdapter;

    public static ActorsListFragment newInstance() {
        return new ActorsListFragment();
    }

    /**
     * Lifecycle method
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.actors_list_fragment, container, false);
        ButterKnife.bind(this, root);
        Injector.get().inject(this);
        return root;
    }

    /**
     * Method to setup recycler view
     */
    private void setupRecyclerView() {
        mActorRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new ActorListRecyclerAdapter(mPresenter, getContext());
        mActorRecyclerView.setAdapter(mAdapter);
    }

    /**
     * Lifecycle method
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mPresenter = new ActorListPresenter(mActorsService, this);
        setupRecyclerView();
        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            mPresenter.getActorsList();
        });
    }

    /**
     * Lifecycle method
     */
    @Override
    public void onResume() {
        super.onResume();
        mPresenter.getActorsList();
    }

    /**
     * Method to load actor list into recycler view
     * @param actorList response
     */
    @Override
    public void loadActors(ActorsResponse actorList) {
        hideLoading();
        stopRefreshing();
        mAdapter.displayActors(actorList.getmActor());
        mAdapter.notifyDataSetChanged();
    }

    /**
     * Open actor details screen
     * @param mActor data
     */
    @Override
    public void openActorDetails(Actor mActor) {
        startActivity(new Intent(getContext(), SingleFragmentActivity.class)
                .putExtra(Actor.class.getSimpleName(), mActor));
    }

    /**
     * Stops refreshing the swipe refresh layout
     */
    private void stopRefreshing() {
        // Stop refresh animation
        mSwipeRefreshLayout.setRefreshing(false);
    }

    /**
     * Show a loading view with a progress bar indicating a loading process.
     */
    @Override
    public void showLoading() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    /**
     * Hide a loading view.
     */
    @Override
    public void hideLoading() {
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    /**
     * Display error in snackbar
     * @param errorMessage string
     */
    @Override
    public void displayErrorResponse(String errorMessage) {
        hideLoading();
        stopRefreshing();
        Snackbar.make(mRootView, errorMessage, Snackbar.LENGTH_LONG).show();
        Log.e(TAG, errorMessage);
    }
}
