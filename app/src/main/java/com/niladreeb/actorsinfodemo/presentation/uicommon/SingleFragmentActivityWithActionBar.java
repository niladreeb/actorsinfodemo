package com.niladreeb.actorsinfodemo.presentation.uicommon;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.niladreeb.actorsinfodemo.R;
import com.niladreeb.actorsinfodemo.presentation.ui.actorslist.ActorsListFragment;

/**
 * Created by niladreeb on 24/11/2017.
 * Container activity with actionbar
 */

public class SingleFragmentActivityWithActionBar extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_fragment_activity);
        initFragment(ActorsListFragment.newInstance());
    }

    /**
     * Initialize fragment
     * @param detailFragment
     */
    private void initFragment(Fragment detailFragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.contentFrame, detailFragment);
        transaction.commit();
    }
}
