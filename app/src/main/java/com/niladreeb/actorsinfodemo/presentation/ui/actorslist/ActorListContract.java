package com.niladreeb.actorsinfodemo.presentation.ui.actorslist;


import com.niladreeb.actorsinfodemo.model.Actor;
import com.niladreeb.actorsinfodemo.model.ActorsResponse;
import com.niladreeb.actorsinfodemo.presentation.presenter.BasePresenter;
import com.niladreeb.actorsinfodemo.presentation.view.BaseView;

/**
 * Created by niladreeb on 24/11/2017.
 */

public interface ActorListContract {

    interface View extends BaseView<Presenter> {
        /**
         * Display actors
         * @param actorList list
         */
        void loadActors(ActorsResponse actorList);

        /**
         * Open actor details page
         * @param mActor object
         */
        void openActorDetails(Actor mActor);

    }

    interface Presenter extends BasePresenter {
        /**
         * Method to get actor list
         */
        void getActorsList();

        /**
         * Handle actor item click
         * @param mActor object
         */
        void actorItemClicked(Actor mActor);

    }
}
