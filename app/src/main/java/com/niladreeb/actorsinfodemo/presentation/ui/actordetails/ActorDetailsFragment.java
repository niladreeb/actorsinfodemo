package com.niladreeb.actorsinfodemo.presentation.ui.actordetails;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.niladreeb.actorsinfodemo.R;
import com.niladreeb.actorsinfodemo.model.Actor;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by niladreeb on 24/11/2017.
 */

public class ActorDetailsFragment extends Fragment {

    @BindView(R.id.actor_image)
    CircleImageView mActorImageView;
    @BindView(R.id.actor_description)
    TextView mDescription;
    @BindView(R.id.actor_dob)
    TextView mDob;
    @BindView(R.id.actor_country)
    TextView mCountry;
    @BindView(R.id.actor_height)
    TextView mHeight;
    @BindView(R.id.actor_spouse)
    TextView mSpouse;
    @BindView(R.id.actor_children)
    TextView mChildren;
    @BindView(R.id.collapsingToolbarLayout)
    CollapsingToolbarLayout mCollapsingToolbarLayout;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    private Actor mActor;

    public static ActorDetailsFragment newInstance() {
        return new ActorDetailsFragment();
    }

    /**
     * Lifecycle method
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.actor_details_fragment, container, false);
        ButterKnife.bind(this, root);
        mActor = (Actor) getActivity().getIntent().getSerializableExtra(Actor.class.getSimpleName());
        return root;
    }

    /**
     * Lifecycle method
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mCollapsingToolbarLayout.setTitle(mActor.getmName());
        Picasso.with(getContext()).load(mActor.getmImage()).into(mActorImageView);
        mDescription.setText(mActor.getmDescription());
        mDob.setText(String.format(getString(R.string.actor_dob), mActor.getmDob()));
        mCountry.setText(String.format(getString(R.string.actor_country), mActor.getmCountry()));
        mHeight.setText(String.format(getString(R.string.actor_height), mActor.getmHeight()));
        mSpouse.setText(String.format(getString(R.string.actor_spouse), mActor.getmSpouse()));
        mChildren.setText(String.format(getString(R.string.actor_children), mActor.getmChildren()));
    }
}
