package com.niladreeb.actorsinfodemo.presentation.ui.actorslist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.niladreeb.actorsinfodemo.R;
import com.niladreeb.actorsinfodemo.model.Actor;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by niladreeb on 24/11/2017.
 */

public class ActorItemView extends RecyclerView.ViewHolder{

    @BindView(R.id.actor_name)
    TextView mActorNameTextView;
    @BindView(R.id.actor_image)
    CircleImageView mActorImageView;
    @BindView(R.id.actor_dob)
    TextView mActorDobTextView;

    private Context mContext;

    /**
     * Constructor
     * @param mItemView
     * @param mContext
     */
    public ActorItemView(View mItemView, Context mContext) {
        super(mItemView);
        this.mContext = mContext;
        ButterKnife.bind(this, itemView);
    }

    /**
     * Bind data to view
     * @param mActor object
     */
    public void bind(Actor mActor) {
        mActorNameTextView.setText(mActor.getmName());
        mActorDobTextView.setText(String.format(mContext.getString(R.string.actor_dob), mActor.getmDob()));
        Picasso.with(mContext).load(mActor.getmImage()).into(mActorImageView);
    }
}
