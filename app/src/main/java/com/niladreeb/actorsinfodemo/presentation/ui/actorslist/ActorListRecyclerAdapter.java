package com.niladreeb.actorsinfodemo.presentation.ui.actorslist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.niladreeb.actorsinfodemo.R;
import com.niladreeb.actorsinfodemo.model.Actor;

import java.util.List;

/**
 * Created by niladreeb on 24/11/2017.
 */

public class ActorListRecyclerAdapter extends RecyclerView.Adapter<ActorItemView> {

    private List<Actor> mActorList;
    private ActorListContract.Presenter mPresenter;
    private Context mContext;

    /**
     * Constructor
     */
    public ActorListRecyclerAdapter(ActorListContract.Presenter mPresenter, Context mContext) {
        this.mPresenter = mPresenter;
        this.mContext = mContext;
    }

    /**
     * Method to create item view
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public ActorItemView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.actor_item, parent, false);
        return new ActorItemView(view, mContext);
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the {@link ActorItemView} to reflect the item at the given
     * position.
     *
     * @param holder     The ViewHolder which should be updated to represent the contents of the
     *                   item at the given position in the data set.
     * @param position   The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(ActorItemView holder, int position) {
        Actor actor = mActorList.get(holder.getAdapterPosition());
        holder.bind(actor);
        holder.itemView.setOnClickListener(v -> mPresenter.actorItemClicked(actor));
    }

    /**
     * Method to return item count
     * @return item count
     */
    @Override
    public int getItemCount() {
        if (mActorList != null && !mActorList.isEmpty()) {
            return mActorList.size();
        } else {
            return 0;
        }
    }

    /**
     * Load actors in recycler adapter
     * @param mActorList list
     */
    public void displayActors(List<Actor> mActorList) {
        this.mActorList = mActorList;
    }
}
