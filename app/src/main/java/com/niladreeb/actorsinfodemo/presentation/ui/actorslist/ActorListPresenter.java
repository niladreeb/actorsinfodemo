package com.niladreeb.actorsinfodemo.presentation.ui.actorslist;

import com.niladreeb.actorsinfodemo.data.ActorsService;
import com.niladreeb.actorsinfodemo.model.Actor;

import io.reactivex.android.schedulers.AndroidSchedulers;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * Created by niladreeb on 24/11/2017.
 */

public class ActorListPresenter implements ActorListContract.Presenter {

    private ActorListContract.View mView;
    private ActorsService mActorsService;

    /**
     * Constructor
     * @param actorsService
     * @param view
     */
    public ActorListPresenter(final ActorsService actorsService, final ActorListContract.View view) {
        mView = checkNotNull(view, "Actor List View can't be null");
        mActorsService = checkNotNull(actorsService, "Actor List Service can't be null");
    }

    /**
     * Method to get actor list
     */
    @Override
    public void getActorsList() {
        mView.showLoading();
        mActorsService.getActors()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        response -> mView.loadActors(response),
                        error -> mView.displayErrorResponse(error.getMessage())
                );
    }

    /**
     * Method to handle individual actor item click
     * @param mActor object
     */
    @Override
    public void actorItemClicked(Actor mActor) {
        mView.openActorDetails(mActor);
    }

}
