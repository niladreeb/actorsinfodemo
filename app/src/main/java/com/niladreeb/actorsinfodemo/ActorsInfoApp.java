package com.niladreeb.actorsinfodemo;

import android.app.Application;

import com.niladreeb.actorsinfodemo.di.Injector;

/**
 * Created by niladreeb on 24/11/2017.
 */

public class ActorsInfoApp extends Application {

    public static ActorsInfoApp sInstance;

    /**
     * Default constructor
     */
    public ActorsInfoApp() {
        Injector.init(this);
        sInstance = this;
    }

    /**
     * Method to get root context
     * @return {@link ActorsInfoApp} instance
     */
    public static ActorsInfoApp getRootContext() {
        return sInstance;
    }
}
