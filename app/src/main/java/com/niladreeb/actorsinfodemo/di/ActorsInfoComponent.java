package com.niladreeb.actorsinfodemo.di;

/**
 * Created by niladreeb on 24/11/2017.
 */

import com.niladreeb.actorsinfodemo.presentation.ui.actorslist.ActorsListFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ActorsModule.class)
public interface ActorsInfoComponent {
    void inject(ActorsListFragment actorsListFragment);
}
