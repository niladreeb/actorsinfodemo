package com.niladreeb.actorsinfodemo.di;

import android.content.Context;

/**
 * Created by niladreeb on 24/11/2017.
 */

public class Injector {

    private static ActorsInfoComponent sComponent;

    private Injector() {
        throw new AssertionError();
    }

    public static void init(Context context) {
        if (sComponent == null) {
            sComponent = DaggerActorsInfoComponent.builder().actorsModule(new ActorsModule(context)).build();
        }
    }

    public static ActorsInfoComponent get() {
        return sComponent;
    }

    public static void setComponent(ActorsInfoComponent component) {
        sComponent = component;
    }
}
