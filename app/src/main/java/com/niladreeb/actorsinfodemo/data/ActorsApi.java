package com.niladreeb.actorsinfodemo.data;

import com.niladreeb.actorsinfodemo.model.ActorsResponse;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;

/**
 * Created by niladreeb on 24/11/2017.
 */

public interface ActorsApi {

    /**
     * Get list of actors
     * @return {@link ActorsResponse} with all details
     */
    @GET("jsonActors")
    Observable<Response<ActorsResponse>> getActors();

}
