package com.niladreeb.actorsinfodemo.data;

import com.niladreeb.actorsinfodemo.model.ActorsResponse;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

import static com.niladreeb.actorsinfodemo.util.ValidationUtil.checkNotNull;

/**
 * Created by niladreeb on 24/11/2017.
 */

public class ActorsService {

    private static final String TAG = ActorsService.class.getSimpleName();

    private ActorsApi mActorsApi;

    public ActorsService(ActorsApi actorsApi) {
        mActorsApi = checkNotNull(actorsApi, "Api can't be null");
    }

    /**
     * Retrieves the list of actors from the server using Rxjava observable and
     * the result is published.
     * @return {@link ActorsResponse}
     */
    public Observable<ActorsResponse> getActors() {
        return Observable.defer(() -> mActorsApi.getActors())
                .subscribeOn(Schedulers.io())
                .map(Response::body);
    }
}
