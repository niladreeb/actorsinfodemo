package com.niladreeb.actorsinfodemo.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by niladreeb on 24/11/2017.
 */

public class ActorsResponse {

    @SerializedName("actors")
    private List<Actor> mActor;

    public List<Actor> getmActor() {
        return mActor;
    }

    public void setmActor(List<Actor> mActor) {
        this.mActor = mActor;
    }
}
