# README #

# This is a demo actors info application#

**Requirements**

* Android Studio latest stable version 3.0.1
* Build tool version 27.0.1 

**Steps for running project in device/emulator**

* Clone project to local directory
* Open with Android Studio
* Run on emulator or device

**Steps for testing the project**

* Open com (test) > niladreeb > actorsinfodemo > run AllUnitTests

** Features **

* Display list of actors with image and some details
* List of actors can be refreshed
* On click of any item in the list the app opens a detailed view of the actor
* Details page have parallex view, so on scroll the header is hidden and body view scope is increased.

**Scope of Improvements** 

* The architecture of the app can be improved even more. For example a domain layer can be implemented with Usecases. 
* The data layer can be improved to implement caching. A repository class will handle the logic of fetching the data either from network or local storage.
* UI improvements and UI test cases.

*Note:* In the assignment description it is stated "Create the Swift/Android app which Parse the below URL and display result in Tableview formate with custom cell" 
but in Android dynamic list of items are generally displayed using listview or recyclerview, so it is implemented in that way.